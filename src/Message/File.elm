module Message.File exposing (FileData, decodeFile, viewFile)

import Accessibility exposing (Html, a, p, text)
import ApiUtils exposing (httpFromMxc)
import Html.Attributes exposing (class, href, rel, style)
import Json.Decode as JD
import Svg exposing (path, svg)
import Svg.Attributes as S exposing (d, viewBox)


{-| This module contains functions for decoding and rendering m.file message events.
-}
type alias FileData =
    { body : String
    , url : String
    }


decodeFile : JD.Decoder FileData
decodeFile =
    JD.map2
        FileData
        (JD.field "body" JD.string)
        (JD.field "url" JD.string)


viewFile : String -> FileData -> Html msg
viewFile homeserverUrl file =
    let
        link =
            httpFromMxc homeserverUrl file.url

        fileName =
            p
                [ class "cactus-message-file-name" ]
                [ text file.body ]

        fileIcon =
            svg
                [ viewBox "0 0 20 20"
                , S.class "cactus-message-file-icon"
                , style "fill" "currentColor"
                ]
                [ path
                    [ style "fill-rule" "evenodd"
                    , d "M6 2a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2V7.414A2 2 0 0015.414 6L12 2.586A2 2 0 0010.586 2H6zm5 6a1 1 0 10-2 0v3.586l-1.293-1.293a1 1 0 10-1.414 1.414l3 3a1 1 0 001.414 0l3-3a1 1 0 00-1.414-1.414L11 11.586V8z"
                    , style "clip-rule" "evenodd"
                    ]
                    []
                ]
    in
    case link of
        Just httpLink ->
            --
            a
                [ class "cactus-message-file"
                , href httpLink
                , rel "noopener"
                ]
                [ fileIcon
                , fileName
                ]

        Nothing ->
            p [ class "cactus-message-error" ]
                [ text "Error: Could not parse file URL" ]
