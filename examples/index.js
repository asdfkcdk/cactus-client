import { initComments } from "../src/cactus.js";

initComments({
    node: "#my-comments",
    defaultHomeserverUrl: "https://matrix.cactus.chat:8448",
    serverName: "cactus.chat",
    siteName: "my-blog",
    commentSectionId: "example-page", // another id: all-event-types

    pageSize: 10,
    loginEnabled: true,
    guestPostingEnabled: true,
    updateInterval: 30.0,
})