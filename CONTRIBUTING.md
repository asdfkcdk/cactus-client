# Contributing

Thank you for being interested in Cactus Comments! Contributions are very welcome! This document is here to help you get started working on the web client.


## Prerequisites

Install the dependencies.

```sh
npm install
```

If you're using [Nix](https://nixos.org), you can use the `shell.nix` file to get the tools you need. Run `nix-shell` to enter the environment.

For formatting, you might want to install an extension for your editor. If you use vim, I highly recommend the [elm-vim](https://github.com/ElmCast/elm-vim) plugin.


## Developing

Serve the test page `examples/index.html` on your local machine using:

```sh
npm run dev
```

Or on your local network using:

```sh
npm run dev:host
```

It automatically reloads when you make updates to the source code in `src`.

You can change CC's settings in the `examples/index.js`. The comment section `commentSectionId: "example-page"` has many random comments. For testing different message types you might use `all-event-types` insteaad.


## Merge Request

Before posting a merge request, make sure to format and run the tests

```sh
npm run format
npm run test
```

You can check the code coverage and the packages using

```sh
npm run coverage
npm run analyse
```