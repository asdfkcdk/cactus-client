import elmPlugin from 'vite-plugin-elm'
import { resolve } from 'path'

const config = {
  plugins: [elmPlugin()],
  build: {
    lib: {
      entry: resolve(__dirname, 'src/cactus.js'),
      formats: ['es'],
      fileName: 'cactus'
    },
  },
}

export default config